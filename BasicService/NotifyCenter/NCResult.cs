﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BasicService.NotifyCenter
{
    /// <summary>
    /// 消息中心返回结果
    /// </summary>
    public class NCResult
    {
       /// <summary>
       /// 状态码
       /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 提示
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public object response { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int total { get; set; }
    }
}
