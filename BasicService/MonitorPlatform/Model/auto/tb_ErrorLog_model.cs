using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BasicService.MonitorPlatform.Model
{
    /// <summary>
    /// tb_ErrorLog Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_ErrorLog_model
    {
	/*代码自动生成工具自动生成 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
        
        /// <summary>
        /// 错误日志
        /// </summary>
        public string Msg { get; set; }
        
        /// <summary>
        /// 错误堆栈
        /// </summary>
        public string Stack { get; set; }
        
    }
}