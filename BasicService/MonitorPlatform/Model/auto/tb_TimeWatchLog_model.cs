using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BasicService.MonitorPlatform.Model
{
    /// <summary>
    /// tb_TimeWatchLog Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_TimeWatchLog_model
    {
	/*代码自动生成工具自动生成 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// 类型：0 默认，1 sql，2 功能
        /// </summary>
        public Byte Type { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        
        /// <summary>
        /// 耗时
        /// </summary>
        public double? Time { get; set; }
        
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }
        
        /// <summary>
        /// 请求url
        /// </summary>
        public string Url { get; set; }
        
        /// <summary>
        /// 消息日志
        /// </summary>
        public string Msg { get; set; }
        
        /// <summary>
        /// 其他标记备注
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// 请求Ip
        /// </summary>
        public string Lyip { set; get; }

        /// <summary>
        /// 手机唯一标识
        /// </summary>
        public string Dyduniquetag { set; get; }

        /// <summary>
        /// 手机端版本号
        /// </summary>
        public string Dydphoneversion { set; get; }

        /// <summary>
        /// 手机设备号
        /// </summary>
        public string Dydphonedevice { set; get; }

        /// <summary>
        /// 用户token
        /// </summary>
        public string Dydtoken { set; get; }

        /// <summary>
        /// 请求Url
        /// </summary>
        public string RequseUrl { set; get; }

        /// <summary>
        /// 服务器IP
        /// </summary>
        public string ServerIp { get; set; }
    }
}