using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BasicService.MonitorPlatform.Model;

namespace XXF.BasicService.MonitorPlatform.Dal
{

	public partial class tb_ErrorLog_dal
    {
        public virtual bool Add(DbConn PubConn, tb_ErrorLog_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
					
					//创建时间
					new ProcedureParameter("@CreateTime",    model.CreateTime),
					//项目名称
					new ProcedureParameter("@ProjectName",    model.ProjectName),
					//
					new ProcedureParameter("@Url",    model.Url),
					//错误日志
					new ProcedureParameter("@Msg",    model.Msg),
					//错误堆栈
					new ProcedureParameter("@Stack",    model.Stack)   
                };
            int rev = PubConn.ExecuteSql(@"insert into tb_ErrorLog(CreateTime,ProjectName,Url,Msg,Stack)
										   values(@CreateTime,@ProjectName,@Url,@Msg,@Stack)", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_ErrorLog_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
                    
					//创建时间
					new ProcedureParameter("@CreateTime",    model.CreateTime),
					//项目名称
					new ProcedureParameter("@ProjectName",    model.ProjectName),
					//
					new ProcedureParameter("@Url",    model.Url),
					//错误日志
					new ProcedureParameter("@Msg",    model.Msg),
					//错误堆栈
					new ProcedureParameter("@Stack",    model.Stack)
            };
			Par.Add(new ProcedureParameter("@id",  model.ID));

            int rev = PubConn.ExecuteSql("update tb_ErrorLog set CreateTime=@CreateTime,ProjectName=@ProjectName,Url=@Url,Msg=@Msg,Stack=@Stack where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id",  id));

            string Sql = "delete from tb_ErrorLog where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual tb_ErrorLog_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", ProcParType.Int32, 4, id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_ErrorLog s where s.id=@id");
            int rev = PubConn.ExecuteSql(stringSql.ToString(), Par);
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0)
            {
				return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

		public virtual tb_ErrorLog_model CreateModel(DataRow dr)
        {
            return new tb_ErrorLog_model
            {
				
				//
				ID = dr["ID"].Toint(),
				//创建时间
				CreateTime = dr["CreateTime"].ToDateTime(),
				//项目名称
				ProjectName = dr["ProjectName"].Tostring(),
				//
				Url = dr["Url"].Tostring(),
				//错误日志
				Msg = dr["Msg"].Tostring(),
				//错误堆栈
				Stack = dr["Stack"].Tostring()
            };
        }
    }
}