﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Threading;
using System.Text;
using System.Data;
using XXF.Db;

namespace XXF.BasicService.MonitorPlatform.Dal
{
    /// <summary>
    /// 队列处理
    /// </summary>
    public   class QueueDal
    {
        private static  Queue<TimeWatchInfo> TimeWatchQueue=new Queue<TimeWatchInfo>();
        private static  List<TimeWatchInfo> Templist=new List<TimeWatchInfo>();
        private  static Queue<ErrorInfo> errorInfoQueues=new Queue<ErrorInfo>();
        private static List<ErrorInfo> temperrorInfoQueues=new List<ErrorInfo>();
        private static System.Timers.Timer timer = new System.Timers.Timer();
        public static object locker = new object();//添加一个对象作为锁
        private static bool istimerStart = false;

        /// <summary>
        /// 添加耗时队列
        /// </summary>
        /// <param name="timeWatchInfo"></param>
        public static void AddtimeWatchInfoQueue(TimeWatchInfo timeWatchInfo)
        {
            lock (locker)
            {
                TimerOperate();
                TimeWatchQueue.Enqueue(timeWatchInfo);
            }
        }
        /// <summary>
        /// 添加耗时日志
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected static void FQueuedal(object sender, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            try
            {
                if (TimeWatchQueue.Count > 5000)
                {
                    lock (locker)
                    {
                        Templist = new List<TimeWatchInfo>();
                        Templist = TimeWatchQueue.ToList();
                        TimeWatchQueue.Clear();
                    }
                }
                if ((Templist != null && Templist.Count>0))
                {
                    if (!string.IsNullOrWhiteSpace(XXF.Common.XXFConfig.MonitorPlatformConnectionString))
                    {
                        using (var c = Db.DbConfig.CreateConn(Db.DbType.SQLSERVER, XXF.Common.XXFConfig.MonitorPlatformConnectionString))
                        {
                            c.Open();
                            c.BeginTransaction();
                            var timewatchinfoTable = FGetDataTable(Templist);
                            var dict = new Dictionary<string, string>
                            {
                                {"Type", "Type"},
                                {"CreateTime","CreateTime"},
                                {"Time", "Time"},
                                {"ProjectName", "ProjectName"},
                                {"Url", "Url"},
                                {"Msg", "Msg"},
                                {"Tag", "Tag"},
                                {"Dyduniquetag", "Dyduniquetag"},
                                {"Dydphoneversion", "Dydphoneversion"},
                                {"Dydphonedevice", "Dydphonedevice"},
                                {"Dydtoken", "Dydtoken"},
                                {"Lyip", "Lyip"},
                                {"ServerIp","ServerIp"}
                            };
                            c.SqlBulkCopy(timewatchinfoTable, "tb_TimeWatchLog", "", new List<ProcedureParameter>(), dict, 0);
                            c.Commit();
                        }
                    }
                    Templist=new List<TimeWatchInfo>();
                }
            }
            catch (Exception exception)
            {
                Templist = new List<TimeWatchInfo>();
                temperrorInfoQueues = new List<ErrorInfo>();
                TimeWatchQueue = new Queue<TimeWatchInfo>();
                errorInfoQueues=new Queue<ErrorInfo>();
            }
            timer.Enabled = true;
        }

        /// <summary>
        /// 时间操作
        /// </summary>
        public static void TimerOperate()
        {
            lock (timer)
            {
                if (istimerStart) return;
                timer.Interval = 5000;
                timer.Enabled = true;
                timer.Elapsed += FQueuedal;
                istimerStart = true;
            }
        }

        public static DataTable FGetDataTable(List<TimeWatchInfo> temptimewatchinfo)
        {
            if (temptimewatchinfo == null || temptimewatchinfo.Count < 1)
            {
                return new DataTable();
            }
            var dt=new DataTable();
            dt.Columns.Add("Type");
            dt.Columns.Add("CreateTime");
            dt.Columns.Add("Time");
            dt.Columns.Add("ProjectName");
            dt.Columns.Add("Url");
            dt.Columns.Add("Msg");
            dt.Columns.Add("Tag");
            dt.Columns.Add("Dyduniquetag");
            dt.Columns.Add("Dydphoneversion");
            dt.Columns.Add("Dydphonedevice");
            dt.Columns.Add("Dydtoken");
            dt.Columns.Add("Lyip");
            dt.Columns.Add("ServerIp");
            foreach (var timewatchinfo in temptimewatchinfo)
            {
                if(timewatchinfo==null)
                    continue;
                var dr = dt.NewRow();
                dr["Type"] = timewatchinfo.Type;
                dr["CreateTime"] = timewatchinfo.CreateTime;
                dr["Time"] = timewatchinfo.Time;
                dr["ProjectName"] = timewatchinfo.ProjectName;
                dr["Url"] = timewatchinfo.Url;
                dr["Msg"] = timewatchinfo.Msg;
                dr["Tag"] = timewatchinfo.Tag;
                dr["Dyduniquetag"] = timewatchinfo.Dyduniquetag + "";
                dr["Dydphoneversion"] = timewatchinfo.Dydphoneversion + "";
                dr["Dydphonedevice"] = timewatchinfo.Dydphonedevice + "";
                dr["Dydtoken"] = timewatchinfo.Dydtoken + "";
                dr["Lyip"] = timewatchinfo.Lyip + "";
                dr["ServerIp"] = timewatchinfo.ServerIp + "";
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}
